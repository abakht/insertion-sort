Summary

Two sorting algorithms were implemented to sort numbers with both merge sort and insertion sort. Using the metrics file, the number of copies, and numbers of comparisons taking place were checked in a numbers text file.

Analysis

Worst Case
Insertion Sort: n^2
Merge Sort: n*log(n)

Best Case
Insertion Sort: n
Merge Sort: n*log(n)

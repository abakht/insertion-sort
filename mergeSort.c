#include "mySort.h"

void mergeSort(int array[], unsigned int l, unsigned int m, unsigned int r)
{ 
	int i, j, k; 
	int n1 = m - l + 1; 
	int n2 = r - m; 

	/* create temp arrays */
	int L[n1], R[n2]; 

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++) 
		myCopy(&array[l + i], &L[i]);
	for (j = 0; j < n2; j++) 
		myCopy(&array[m + 1 + j], &R[j]);

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0; // Initial index of first subarray 
	j = 0; // Initial index of second subarray 
	k = l; // Initial index of merged subarray 
	while (i < n1 && j < n2) 
	{ 
		if (myCompare(L[i] , R[j]) < 0) 
		{ 
			myCopy(&L[i], &array[k]);
			i++; 
		} 
		else
		{ 
			myCopy(&R[j], &array[k]);
			j++; 
		} 
		k++; 
	} 

	/* Copy the remaining elements of L[], if there 
	are any */
	while (i < n1) 
	{ 
		array[k] = L[i];
		myCopy(&L[i], &array[k]); 
		i++; 
		k++; 
	} 

	/* Copy the remaining elements of R[], if there 
	are any */
	while (j < n2) 
	{ 
		myCopy(&R[j], &array[k]);
		j++; 
		k++; 
	} 
} 

/* l is for left index and r is right index of the 
sub-array of arr to be sorted */
void mySort(int array[], unsigned int first, unsigned int last) 
{ 
	unsigned int l = first;
	unsigned int r = last;
	if (l < r) 
	{ 
		// Same as (l+r)/2, but avoids overflow for 
		// large l and h 
		unsigned int m = l+(r-l)/2;

		// Sort first and second halves 
		mySort(array, l, m); 
		mySort(array, m+1, r); 

		mergeSort(array, l, m, r); 
	} 
} 

#include "mySort.h"

void mySort(int array[], unsigned int first, unsigned int last)
{ 
   int i, key, j; 
   for (i = first; i <= last; i++) 
   { 
       key = array[i]; 
       j = i-1; 
       
       while (j >= 0 && (myCompare(array[j] , key) > 0)) 
       { 
           myCopy(&array[j], &array[j+1]);
           
           j = j-1; 
       } 
       array[j+1] = key; 
   } 
} 
